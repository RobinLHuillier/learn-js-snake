/*------------------------------
Variables globales
*/

//canvas layers
var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
//looping animation
var refresh = 100;
var lastTime = 0;
var timer;
//mouse
const mouse = {
    x: undefined,
    y: undefined,
};
//img
var img = {"trail": new Image(), "fruit": new Image()};
var sound = {"eat": null, "lost": null};
//tiles
var tileSize = 35;

/*------------------------------
Classes
*/

class SnakeNode {
    constructor(x,y,next=null) {
        this.x = x;
        this.y = y;
        this.next = next;
    }
    draw() {
        context.drawImage(img["trail"], this.x*tileSize, this.y*tileSize, tileSize, tileSize);
    }
}

/*------------------------------
Handlers
*/

var gameHandler = {
    lost: false,
    stop: false,

    initialize() {
        this.lost = false;
        this.stop = false;
        //initialize images
        img["trail"].src = "img/snakeTrail.png";
        img["fruit"].src = "img/cherry.png"
        //initialize handlers
        soundHandler.initialize();
        snakeHandler.initialize();
        fruitHandler.initialize();
        //launch main loop
        animate(performance.now());
    },
    lose() {
        soundHandler.play("lost");
        context.font = "50px Georgia";
        context.fillStyle = "red";
        context.fillText("LOSER (" + snakeHandler.snakeLength().toString() + " pts)", 200, 320);
        this.stop = true;
    },
    draw() {
        context.clearRect(0,0,canvas.width,canvas.height);
        snakeHandler.draw();
        fruitHandler.draw();
    },
    core: function() {
        if (this.lost) {
            this.lose();
        } else {
            //core
            snakeHandler.core();
            fruitHandler.core();
            //draw
            this.draw();
        }
    }
};

var snakeHandler = {
    head: new SnakeNode(0,0),
    direction: "right",
    justAte: false,

    initialize() {
        this.head = new SnakeNode(0,0);
        this.direction = "right";
        this.justAte = true;
    },

    keyPressed(key) {
        let keyName = key.key;
        switch(keyName) {
            case "z":
            case "ArrowUp":
                this.direction = "up";
                break;
            case "s":
            case "ArrowDown":
                this.direction = "down";
                break;
            case "q":
            case "ArrowLeft":
                this.direction = "left";
                break;
            case "d":
            case "ArrowRight":
                this.direction = "right";
                break;
        }
    },
    move() {
        // calculate next head position
        let x = this.head.x;
        let y = this.head.y;
        switch(this.direction) {
            case "right":
                x++;
                break;
            case "left":
                x--;
                break;
            case "down":
                y++;
                break;
            case "up":
                y--;
                break;
        }
        // verify the bounds, loop around if necessary
        let maxX = canvas.width/tileSize;
        let maxY = canvas.height/tileSize;
        if (y < 0) {
            y = maxY -1;
        }
        if (x < 0) {
            x = maxX -1;
        }
        if (y === maxY) {
            y = 0;
        }
        if (x === maxX) {
            x = 0;
        }
        // create the newHead
        let newHead = new SnakeNode(x,y);
        newHead.next = this.head;
        this.head = newHead;
        // if just ate, don't delete the last element
        if (this.justAte !== true) {
            let node = this.head;
            while (node.next !== null && node.next.next !== null) {
                node = node.next;
            }
            node.next = null;
        }
        this.justAte = false;
        
        this.didIDie();
    },
    isThereASnakeHere(x,y) {
        let node = this.head;
        while (node !== null && (node.x !== x || node.y !== y)) {
            node = node.next;
        }
        return (node!==null); 
    },
    didIAte() {
        if (fruitHandler.isThereAFruitHere(this.head.x,this.head.y)) {
            this.justAte = true;
            soundHandler.play("eat");
        }
    },
    didIDie() {
        let x = this.head.x;
        let y = this.head.y;
        let node = this.head.next;
        while (node !== null) {
            if (node.x === x && node.y === y) {
                gameHandler.lost = true;
                return;
            }
            node = node.next;
        }
    },
    snakeLength() {
        let length = 0;
        let node = this.head;
        while (node !== null) {
            length++;
            node = node.next;
        }
        return length;
    },

    draw() {
        let current = this.head;
        while (current !== null) {
            current.draw();
            current = current.next;
        }
    },
    core() {
        this.move();
        this.didIAte();
    }
};

var fruitHandler = {
    maxFruits: 2,
    fruitDisplay: new Array(),

    initialize() {
        this.maxFruits = 2;
        fruitDisplay = new Array();
    },
    spawnFruit() {
        if (this.fruitDisplay.length < this.maxFruits) {
            let posXMax = canvas.width/tileSize;
            let posYMax = canvas.height/tileSize;
            let x = Math.floor(Math.random() * posXMax);
            let y = Math.floor(Math.random() * posYMax);
            while (snakeHandler.isThereASnakeHere(x,y)) {
                x = Math.floor(Math.random() * posXMax);
                y = Math.floor(Math.random() * posYMax);
            }
            this.fruitDisplay.push({"x":x,"y":y});
        }
    },
    isThereAFruitHere(x,y) {
        for(let i=0; i<this.fruitDisplay.length; i++) {
            if (this.fruitDisplay[i].x === x && this.fruitDisplay[i].y === y) {
                this.fruitDisplay.splice(i,1);
                return true;
            }
        }
        return false;
    },

    draw() {
        for(let i=0; i<this.fruitDisplay.length; i++) {
            let x = this.fruitDisplay[i].x;
            let y = this.fruitDisplay[i].y;
            context.drawImage(img.fruit,tileSize*x, tileSize*y, tileSize, tileSize);
        }
    },
    core() {
        this.spawnFruit();
    },
}

var soundHandler = {

    initialize() {
        sound.eat = new Audio("sound/eat.ogg");
        sound.lost = new Audio("sound/damage.ogg");
    },
    play(name) {
        sound[name].play();
    }
}

/*------------------------------
Animation and interactions
*/

function animate(timestamp) {
    if(lastTime == 0) lastTime = timestamp-1;
    if(timestamp - lastTime >= refresh || timestamp - lastTime == 0) {
        //console.time("core");
        gameHandler.core();
        //console.timeEnd("core");
        timer += timestamp - lastTime;
        lastTime = timestamp;
    }
    if (!gameHandler.stop) {
        window.requestAnimationFrame(animate);
    }
}

document.addEventListener('mousemove', e => {
    canvasPosition = canvas.getBoundingClientRect();
    mouse.x = Math.round(e.x - canvasPosition.left);
    mouse.y = Math.round(e.y - canvasPosition.top);
    //what to do with that?
});

document.addEventListener('mousedown', e => {mouseDown()});
document.addEventListener('mouseup', e => {mouseUp()});
document.addEventListener('keydown', e => {keyDown(e)});

function keyDown(key) {
    snakeHandler.keyPressed(key);
}

/*------------------------------
Page Start
*/

window.onload = function() {
    gameHandler.initialize()
}