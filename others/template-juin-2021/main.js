/*------------------------------
TODOS



/*------------------------------
Variables globales
*/ 

//canvas
var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
canvas.width = 700;
canvas.height = 700;
//mouse
var canvasPosition = canvas.getBoundingClientRect();
const mouse = {
    x: undefined,
    y: undefined,
};
//time
var refresh = 50;
var lastTime = 0;
var timer;
//sizes and placement
var tileSize = 60;
var offsetX = 110;
var offsetY = 110;
//exit conditions
var exit = false;
var lost = false;
var timerExit = 10;
var exitMenu = false;

/*------------------------------
Initialisation
*/

function initializeImg() {
    
}

/*------------------------------
Classes / Algos
*/



/*------------------------------
Animation et affichage
*/

function drawScreen() {
    //effacer
    context.clearRect(0, 0, canvas.width, canvas.height);

}

function animate(timestamp) {
    if(lastTime == 0) lastTime = timestamp-1;
    if(timestamp - lastTime >= refresh || timestamp - lastTime == 0) { 
        if(!exit) {
            //console.time("core");
            


            //console.timeEnd("core");
        } else {
            timerExit -= refresh;
        }
        timer += timestamp - lastTime;
        lastTime = timestamp;
    }
    if(exit) {
        if(lost) text = "Défaite!";
        else text = "Victoire!";
        context.font = "100px Arial";
        context.fillStyle = "red";
        context.textAlign = "center";
        context.fillText(text, canvas.width/2, canvas.height/2);
        if(timerExit <= 0) {
            menu();
        }
    }
    if(!exit || timerExit > 0) {
        window.requestAnimationFrame(animate);
    }
}

/*------------------------------
Interaction utilisateur
*/

canvas.addEventListener('mousemove', e => {
    canvasPosition = canvas.getBoundingClientRect();
    mouse.x = Math.round(e.x - canvasPosition.left);
    mouse.y = Math.round(e.y - canvasPosition.top);
});

canvas.addEventListener('click', e => {mouseClic()});

function mouseClic() {
   
}


/*------------------------------
Menu
*/

function menu() {
    exit = false;
    lastTime = 0;
    exitMenu = false;
    menuAnimation(performance.now());
}

function menuAnimation(timestamp) {
    if(lastTime == 0) lastTime = timestamp-1;
    if(timestamp - lastTime >= refresh || timestamp - lastTime == 0) { 
        lastTime = timestamp;
        if(!exitMenu) {
            



        }
    }
    if(!exitMenu) {
        window.requestAnimationFrame(menuAnimation);
    }
}

/*------------------------------
Démarrage de la page
*/

window.onload = function() {
    
    menu();
}