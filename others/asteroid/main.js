/*------------------------------
Variables globales
*/
//looping animation
var refresh = 25;
var lastTime = 0;
var timer;
//canvas layers
var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
//mouse
var canvasPosition = canvas.getBoundingClientRect();
const mouse = {
    x: undefined,
    y: undefined,
};
//img
var img = {"hero":null, "enemy":null, "fire":null, "planet1":null, "planet2": null, "planet3": null, "star1": null, "star2": null, "star3": null, "heroleft": null, "heroright": null, "enemyleft": null, "enemyright": null, "enemydown": null, "explosion1": null, "explosion2": null, "explosion3": null};
var sound = {"battle":null, "explosion":null, "lost":null, "menu":null, "pew":null};
var shipSize = 40;
var fireSize = 10;

/*------------------------------
Classes
*/

class Ship {
    constructor(x,y,type,sizeX=shipSize,sizeY=shipSize) {
        this.x = x;
        this.y = y;
        this.img = type;
        this.type = type;
        this.direction = null;
        this.speed = 0;
        this.timeToFire = 0;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        switch(type) {
            case "hero":
                this.speedMax = 15;
                this.acceleration = 1;
                this.reload = 20;
                break;
            case "enemy":
            default:
                this.speedMax = 2;
                this.acceleration = 1;
                this.reload = 20;
                break;
        }
    }
    move(direction=this.direction) {
        if (direction === null) {
            this.direction = null;
            this.speed = 0;
        } else {
            if (direction !== this.direction) {
                this.direction = direction;
                this.speed = 0;
            }
            this.speed += this.acceleration;
            if (this.speed > this.speedMax) {
                this.speed = this.speedMax;
            }
            let modifX = 0;
            let modifY = 0;
            switch(this.direction) {
                case "left":
                    modifX = -1;
                    break;
                case "right":
                    modifX = 1;
                    break;
                case "down":
                    modifY = 1;
                    break;
                case "up":
                    modifY = -1;
                    break;
            }
            this.x += this.speed * modifX;
            this.y += this.speed * modifY;
            if(this.x < 0) this.x = 0;
            if(this.y < 0) this.y = 0;
            if(this.x+this.sizeX > canvas.width) this.x = canvas.width-this.sizeX;
            if(this.y+this.sizeY > canvas.height) this.y = canvas.height-this.sizeY;
        }
    }
    fire() {
        if(this.timeToFire == 0) {
            this.timeToFire = this.reload;
            return true;
        }
        return false;
    }
    core() {
        if(this.timeToFire > 0) this.timeToFire--;
    }
    draw() {
        let ajout = "";
        if(this.direction !== null) {
            ajout = this.direction;
        }
        context.drawImage(img[this.img+ajout],this.x,this.y,this.sizeX,this.sizeY);
    }
}

class Fire {
    constructor(x,y,direction,type="normal",sizeX=fireSize,sizeY=fireSize) {
        this.x = x;
        this.y = y;
        if (direction == "up") {
            this.direction = -1;
        } else {
            this.direction = 1;
        }
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        switch(type) {
            default:
            case "normal":
                this.img = "fire";
                this.speed = 5;
                break;
        }
    }
    core() {
        this.y += this.speed * this.direction;
        if (this.y < 0 || this.y + this.sizeY > canvas.height) return true;
        return false;
    }
    draw() {
        context.drawImage(img[this.img],this.x,this.y,this.sizeX,this.sizeY);
    }
}

class Terrain {
    constructor(type,y=0) {
        let num = 1+Math.floor(Math.random()*3);
        this.img = type+num.toString();
        let size = 0;
        this.type = type;
        switch(type) {
            case "planet":
                size = 51+Math.floor(Math.random()*50);
                break;
            case "star":
                size = 10+Math.floor(Math.random()*6);
                break;
        }
        this.sizeX = size;
        this.sizeY = size;
        this.x = Math.floor(Math.random()*(canvas.width+200)) - 150;
        this.y = canvas.height - y;
        this.speed = 1+Math.floor(Math.random()*5);
    }
    move() {
        this.y -= this.speed;
        if(this.y < -100) return true;
        return false;
    }
    draw() {
        context.drawImage(img[this.img],this.x,this.y,this.sizeX,this.sizeY);
    }
}

class Explosion {
    constructor(x,y) {
        this.x = x;
        this.y = y;
        this.num = 1;
        this.img = "explosion";
    }
    core() {
        this.num++;
        return this.num===4;
    }
    draw() {
        context.drawImage(img[this.img+this.num.toString()],this.x,this.y);
    }
}

/*------------------------------
Handlers
*/

var gameHandler = {
    assetsLoaded: 0,
    state: "loading", //loading, playing

    initialize(re=false) {
        if (!re) {
            this.assetsLoaded = 0;
            this.state = "loading";
            this.initializeAssets();
        }
        heroHandler.initialize();
        fireHandler.initialize();
        enemyHandler.initialize();
        soundHandler.initialize();
        terrainHandler.initialize();

        animate(performance.now());
    },
    initializeAssets: function() {
        imgKey = Object.keys(img);
        for(let i=0; i<imgKey.length; i++) {
            key = imgKey[i];
            img[key] = new Image();
            img[key].src = "img/"+key+".png";
            img[key].onload = this.assetsLoading();
        }
        soundKey = Object.keys(sound);
        for(let i=0; i<soundKey.length; i++) {
            key = soundKey[i]
            sound[key] = new Audio("sound/"+key+".mp3");
            sound[key].onload = this.assetsLoading();
        }
    },
    assetsLoading: function() {
        this.assetsLoaded++;
    },
    allAssetsLoaded: function() {
        return this.assetsLoaded == (Object.keys(img).length + Object.keys(sound).length);
    },
    draw() {
        //erase
        context.clearRect(0,0,canvas.width,canvas.height);
        //draw
        terrainHandler.draw();
        heroHandler.draw();
        fireHandler.draw();
        enemyHandler.draw();
    },
    core: function() {
        if(this.state === "loading") {
            if(this.allAssetsLoaded()) {
                this.state = "playing";
                soundHandler.play("battle");
            } else {
                //show loading advancement
            }
        } else {
            //core
            heroHandler.core();
            fireHandler.core();
            enemyHandler.core();
            terrainHandler.core();
            //draw
            this.draw();
        }
    }
};

var heroHandler = {
    heroShip: new Ship(330, 600, "hero", shipSize, shipSize),
    direction: null,
    firing: false,

    initialize() {
        this.heroShip = new Ship(330, 600, "hero", shipSize, shipSize);
        this.direction = null;
        this.firing = false;
    },
    keyPressed(key) {
        switch(key) {
            case "ArrowLeft":
            case "q":
                this.direction = "left";
                break;
            case "ArrowRight":
            case "d":
                this.direction = "right";
                break;
            case " ":
                this.firing = true;
                break;
        }
    },
    keyReleased(key) {
        switch(key) {
            case "ArrowLeft":
            case "q":
                if (this.direction === "left") this.direction = null;
                break;
            case "ArrowRight":
            case "d":
                if (this.direction === "right") this.direction = null;
                break;
            case " ":
                this.firing = false;
                break;
        }
    },

    draw() {
        this.heroShip.draw();
    },
    core() {
        this.heroShip.move(this.direction);
        if(this.firing && this.heroShip.fire()) {
            let y = this.heroShip.y;
            let x = this.heroShip.x + Math.floor(this.heroShip.sizeX/2);
            fireHandler.addFire(x,y,"up");
        }
        this.heroShip.core();
    },
}

var enemyHandler = {
    listEnemies: new Array(),
    moveDirection: "right",
    moveNum: 50,
    moveQuantity: 100,

    initialize() {
        this.listEnemies = new Array();
        for(let i=110;i<=530;i+=60) {
            for(let j=10;j<=310;j+=60) {
                let enemy = new Ship(i,j,"enemy");
                this.listEnemies.push(enemy);
            }
        }
        this.moveDirection = "right";
        this.moveNum = 50;
        this.moveQuantity = 100;
    },
    remove(enemy) {
        this.listEnemies = this.listEnemies.filter(e => {return e!==enemy});
    },
    amIShot(x,y,w,h) {
        for(let i=0; i<this.listEnemies.length; i++) {
            let enemy = this.listEnemies[i];
            if(doesItCollide(x,y,w,h,enemy.x,enemy.y,enemy.sizeX,enemy.sizeY)) {
                return enemy;
            }
        }
        return null;
    },
    move() {
        for(let i=0; i<this.listEnemies.length; i++) {
            let enemy = this.listEnemies[i];
            enemy.move(this.moveDirection);
        }
        this.moveNum++;
        if (this.moveNum >= this.moveQuantity) {
            this.moveNum = 0;
            switch(this.moveDirection) {
                case "right":
                    this.moveDirection = "down";
                    this.moveNum = Math.floor(this.moveQuantity*9/10);
                    break;
                case "down":
                    this.moveDirection = "left";
                    break;
                case "left":
                    this.moveDirection = "right";
                    break;
            }
        }
    },

    draw() {
        for(let i=0; i<this.listEnemies.length; i++) {
            this.listEnemies[i].draw();
        }
    },
    core() {
        this.move();
    }
}

var fireHandler = {
    listFire: new Array(),
    listExplode: new Array(),

    initialize() {
        listFire = new Array();
        listExplode = new Array();
    },
    addFire(x,y,direction,type="normal") {
        let fire = new Fire(x,y,direction,type);
        this.listFire.push(fire);
        soundHandler.play("pew");
    },
    collide(fire) {
        if(fire.direction === -1) {
            //is enemy shot?
            return enemyHandler.amIShot(fire.x,fire.y,fire.sizeX,fire.sizeY);
        }
        return null;
    },

    draw() {
        for(let i=0;i<this.listFire.length;i++) {
            this.listFire[i].draw();
        }
        for(let i=0;i<this.listExplode.length;i++) {
            this.listExplode[i].draw();
        }
    },
    core() {
        let suppr = new Array();
        for(let i=0; i<this.listExplode.length; i++) {
            if(this.listExplode[i].core()) {
                suppr.push(this.listExplode[i]);
            }
        }
        this.listExplode = this.listExplode.filter(explosion => {return suppr.indexOf(explosion)===-1});
        suppr = new Array();
        for(let i=0;i<this.listFire.length;i++) {
            let fire = this.listFire[i];
            if(fire.core()) {   //exit the screen
                suppr.push(fire);
            } else {
                let res = this.collide(fire);
                if(res !== null) {
                    if(fire.direction === -1) {
                        //strike enemy
                        enemyHandler.remove(res);
                        soundHandler.play("explosion");
                        this.listExplode.push(new Explosion(res.x,res.y));
                    } else {
                        //strike player
                    }
                    suppr.push(fire);
                }
            }
        }
        this.listFire = this.listFire.filter(fire => {return suppr.indexOf(fire)===-1});
    },
}

var terrainHandler = {
    listTerrain: new Array(),

    initialize() {
        this.listTerrain = new Array();
        for(let i=0; i<3; i++) {
            let terrain = new Terrain("planet",350*i);
            this.listTerrain.push(terrain);
        }
        for(let i=0; i<35; i++) {
            let terrain = new Terrain("star",20*i);
            this.listTerrain.push(terrain);
        }
    },

    draw() {
        context.fillStyle = 'black';
        context.fillRect(0,0,canvas.width,canvas.height);
        for(let i=0; i<this.listTerrain.length; i++) {
            this.listTerrain[i].draw();
        }
    },
    core() {
        for(let i=0; i<this.listTerrain.length; i++) {
            let terrain = this.listTerrain[i]
            if (terrain.move()) {
                let type = terrain.type;
                this.listTerrain[i] = new Terrain(type);
            }
        }
    }
}

var soundHandler = {
    initialize() {
        
    },
    play(name) {
        sound[name].currentTime = 0;
        sound[name].play();
    }
};

/*------------------------------
Algos
*/

function doesItCollide(x1,y1,w1,h1,x2,y2,w2,h2) {
    return !(x1 + w1 < x2 || x2 + w2 < x1 || y1 + h1 < y2 || y2 + h2 < y1);
}

/*------------------------------
Animation and interactions
*/

function animate(timestamp) {
    if(lastTime == 0) lastTime = timestamp-1;
    if(timestamp - lastTime >= refresh || timestamp - lastTime == 0) {
        //console.time("core");
        gameHandler.core();
        //console.timeEnd("core");
        timer += timestamp - lastTime;
        lastTime = timestamp;
    }
    window.requestAnimationFrame(animate);
}

document.addEventListener('keydown', e => {keyDown(e)});
document.addEventListener('keyup', e => {keyUp(e)});

function keyDown(key) {
    heroHandler.keyPressed(key.key);
}

function keyUp(key) {
    heroHandler.keyReleased(key.key);
}

document.addEventListener('mousemove', e => {
    canvasPosition = canvas.getBoundingClientRect();
    mouse.x = Math.round(e.x - canvasPosition.left);
    mouse.y = Math.round(e.y - canvasPosition.top);
    //what to do with that?
});
document.addEventListener('mousedown', e => {mouseDown()});
document.addEventListener('mouseup', e => {mouseUp()});

function mouseDown() {

}

function mouseUp() {

}

/*------------------------------
Page Start
*/

window.onload = function() {
    gameHandler.initialize();
}