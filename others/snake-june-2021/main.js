/*------------------------------
Variables globales
*/

var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');

var refresh = 100;
var lastTime = 0;
var timer;

var mat;
var tileSize = 25;
var offsetX = 50;
var canvasSize = 700;

var imgGround, imgCherry, imgRemains, imgEmptyHeart, imgFullHeart, imgTree, imgPowerUp, imgSnakeHeadDown, imgSnakeHeadUp, imgSnakeHeadLeft, imgSnakeHeadRight, imgSnakeHeadInjuredDown, imgSnakeHeadInjuredRight, imgSnakeHeadInjuredLeft, imgSnakeHeadInjuredUp, imgSnakeTrail, imgSnakeTrailInjured, imgSnakeTrailPower;

var cherryGenerated = false;
var maxCherries = 4;
var actualCherries = 0;
var plantedTrees;
var treesToPlant;

var tickUntilCherry = 100;
var tickSinceEaten = 0;
var tickSincePowerUp = 0;
var timeToPoop = 10;
var timeToTree = 30;

var life = 2;
var lifeMax = 2;

var exit = false;
var exitMenu;
var lost = false;
var timerExit = 10;

const maxLevel = 8;
var unlockedLevel = 1;
var actualLevel;

var bestScore = -1;
var textPowerUp = "placeholder";
var timerTextPowerUp;

/*------------------------------
Initialisation
*/

function initializeScreen() {
    document.body.innerHTML = "<canvas id='canvas' width='" + canvasSize + "' height='" + canvasSize + "' style='position:absolute; left:0; top:0'></canvas>";
    canvas = document.getElementById('canvas');
    context = canvas.getContext('2d');
    document.addEventListener('contextmenu', event => event.preventDefault());
    window.onkeydown = k => {keyOnImg(k.code)};
}

function initializeStats() {
    timer = 0;
    actualCherries = 1;
    exit = false;
    mat = new Array();
    for(let i=0; i<(canvasSize/tileSize); i++) {
        mat[i] = new Array();
        for(let j=0; j<((canvasSize-offsetX)/tileSize); j++) {
            mat[i][j] = "";
            if(actualLevel == 8 && (i < 3 || j < 3 || i > (canvasSize/tileSize - 4) || j > ((canvasSize-offsetX)/tileSize - 4))) {
                mat[i][j] = "tree";
            }
        }
    }
    tickSinceEaten = 0;
    tickSincePowerUp = 0;
    cherryGenerated = false;
    snakeChain.initialize();
    poop.initialize();
    initializeScreen();
}

function initializeImg() {
    imgGround = new Image();
    imgCherry = new Image();
    imgRemains = new Image(); 
    imgEmptyHeart  = new Image(); imgFullHeart = new Image(); imgTree = new Image(); imgPowerUp = new Image(); imgSnakeHeadDown = new Image(); imgSnakeHeadUp = new Image(); imgSnakeHeadLeft = new Image(); imgSnakeHeadRight = new Image(); imgSnakeHeadInjuredDown = new Image(); imgSnakeHeadInjuredRight = new Image(); imgSnakeHeadInjuredLeft = new Image(); imgSnakeHeadInjuredUp = new Image(); imgSnakeTrail = new Image(); imgSnakeTrailInjured = new Image();
    imgSnakeTrailPower = new Image();
    imgGround.src = "img/grassCenter.png"; 
    imgCherry.src = "img/cherry.png"; imgRemains.src = "img/foodRemains.png"; imgEmptyHeart.src = "img/heartEmpty.png"; imgFullHeart.src = "img/heartFull.png"; imgTree.src = "img/pineSapling.png"; imgPowerUp.src = "img/powerUp.png"; imgSnakeHeadDown.src = "img/snakeHeadDown.png"; imgSnakeHeadUp.src = "img/snakeHeadUp.png"; imgSnakeHeadLeft.src = "img/snakeHeadLeft.png"; imgSnakeHeadRight.src = "img/snakeHeadRight.png"; imgSnakeHeadInjuredDown.src = "img/snakeHeadInjuredDown.png"; imgSnakeHeadInjuredRight.src = "img/snakeHeadInjuredRight.png"; imgSnakeHeadInjuredLeft.src = "img/snakeHeadInjuredLeft.png"; imgSnakeHeadInjuredUp.src = "img/snakeHeadInjuredUp.png"; imgSnakeTrail.src = "img/snakeTrail.png"; imgSnakeTrailInjured.src = "img/snakeTrailInjured.png";
    imgSnakeTrailPower.src = "img/snakeTrailPower.png";
}

function initializeLvl(lvl) {
    tickUntilCherry = 100;
    actualLevel = lvl;
    plantedTrees = 0;
    timeToPoop = 10;
    switch(lvl) {
        case 1:
            tileSize = 50;        
            life = 5;
            lifeMax = 5;
            refresh = 250;
            treesToPlant = 5;
            break;
        case 2:
            tileSize = 50;
            life = 5;
            lifeMax = 5;
            refresh = 200;
            treesToPlant = 10;
            break;
        case 3:
            tileSize = 50;
            life = 4;
            lifeMax = 4;
            refresh = 200;
            treesToPlant = 10;
            timeToPoop = 1;
            break;
        case 4:
            tileSize = 50;
            life = 5;
            lifeMax = 5;
            refresh = 50;
            treesToPlant = 5;
            break;
        case 5:
            tileSize = 50;
            life = 1;
            lifeMax = 5;
            refresh = 150;
            treesToPlant = 20;
            break;
        case 6:
            tileSize = 25;
            life = 3;
            lifeMax = 3;
            refresh = 150;
            treesToPlant = 5;
            break;
        case 7:
            tileSize = 25;
            life = 5;
            lifeMax = 5;
            refresh = 150;
            treesToPlant = 80;
            break;
        case 8:
            tileSize = 25;
            life = 3;
            lifeMax = 3;
            refresh = 150;
            treesToPlant = 20;
            break;
        case 9:
            tileSize = 25;
            life = 2;
            lifeMax = 2;
            refresh = 150;
            treesToPlant = 0;
            powerUp.initialize();
            break;
    }
}

/*------------------------------
Animation et affichage
*/

function drawScreen() {
    //effacer
    context.fillStyle = "green";
    context.fillRect(0, 0, 700, 700);
    //afficher le terrain
    for(let i=0; i<mat.length; i++) {
        for(let j=0; j<mat[i].length; j++) {
            context.drawImage(imgGround, tileSize*i, tileSize*j+offsetX, tileSize, tileSize);
            switch(mat[i][j]) {
                case "tree":
                    context.drawImage(imgTree, tileSize*i, tileSize*j+offsetX, tileSize, tileSize);
                    break;
                case "poop":
                    context.drawImage(imgRemains, tileSize*i, tileSize*j+offsetX, tileSize, tileSize);
                    break;
                case "cherry":
                    context.drawImage(imgCherry, tileSize*i, tileSize*j+offsetX, tileSize, tileSize);
                    break;
                case "powerup":
                    context.drawImage(imgPowerUp, tileSize*i, tileSize*j+offsetX, tileSize, tileSize);
            }
        }
    }
    let node = snakeChain.first;
    if(snakeChain.directionX == 1) {
        context.drawImage(imgSnakeHeadRight, tileSize*node.x, tileSize*node.y+offsetX, tileSize, tileSize);
    } else if(snakeChain.directionX == -1) {
        context.drawImage(imgSnakeHeadLeft, tileSize*node.x, tileSize*node.y+offsetX, tileSize, tileSize);
    } else if(snakeChain.directionY == -1) {
        context.drawImage(imgSnakeHeadUp, tileSize*node.x, tileSize*node.y+offsetX, tileSize, tileSize);
    } else {
        context.drawImage(imgSnakeHeadDown, tileSize*node.x, tileSize*node.y+offsetX, tileSize, tileSize);
    }
    while(node.next != null) {
        node = node.next;
        if(!snakeChain.outch) {
            if(actualLevel == 9 && powerUp.onGoing) {
                context.drawImage(imgSnakeTrailPower, tileSize*node.x, tileSize*node.y+offsetX, tileSize, tileSize);
            } else {
                context.drawImage(imgSnakeTrail, tileSize*node.x, tileSize*node.y+offsetX, tileSize, tileSize);
            }
        } else {
            context.drawImage(imgSnakeTrailInjured, tileSize*node.x, tileSize*node.y+offsetX, tileSize, tileSize);
        }
    }
    if(snakeChain.outch) {
        snakeChain.outch = false;
    }
    context.font = (offsetX-10) + "px Arial";
    context.fillStyle = "black";
    context.textAlign = "left";
    if(actualLevel == 9) {
        context.fillText(plantedTrees + " Trees", 5, offsetX-15);
    } else {
        context.fillText(plantedTrees + " / " + treesToPlant + " Trees", 5, offsetX-15);
    }
    let life2 = life;
    for(let i=lifeMax; i>0; i--) {
        if(life2>0) {
            context.drawImage(imgFullHeart, 690-offsetX*i, 5, offsetX-10, offsetX-10);
        } else {
            context.drawImage(imgEmptyHeart, 690-offsetX*i, 5, offsetX-10, offsetX-10);
        }
        life2--;
    }
    if(actualLevel == 9 && timerTextPowerUp > 0) {
        context.font = "30px Arial";
        context.fillStyle = "black";
        context.textAlign = "center";
        context.fillText(textPowerUp, canvas.width/2, canvas.height/2);
    }
}

function animate(timestamp) {
    if(lastTime == 0) lastTime = timestamp-1;
    if(timestamp - lastTime >= refresh || timestamp - lastTime == 0) { 
        if(!exit) {
            console.time("core");
            snakeChain.advance();
            generateFood();
            if(actualLevel == 9) {
                powerUp.advance();
            }
            drawScreen();
            console.timeEnd("core");
        } else {
            timerExit -= refresh;
        }
        timer += timestamp - lastTime;
        lastTime = timestamp;
    }
    if(exit) {
		let text;
        if(lost) text = "Défaite!";
        else text = "Victoire!";
        context.font = "100px Arial";
        context.fillStyle = "red";
        context.textAlign = "center";
        context.fillText(text, canvas.width/2, canvas.height/2);
        if(timerExit <= 0) menu();
    }
    if(!exit || timerExit > 0) {
        window.requestAnimationFrame(animate);
    }
}

/*------------------------------
Interaction utilisateur
*/

function clickOnMenu(lvl) {
    exitMenu = true;
    initializeLvl(lvl);
    initializeStats();
    animate(performance.now());
}

function keyOnImg(code) {
    switch(code) {
        case "ArrowLeft":
            snakeChain.directionX = -1;
            snakeChain.directionY = 0;
            break;
        case "ArrowRight":
            snakeChain.directionX = 1;
            snakeChain.directionY = 0;
            break;
        case "ArrowUp":
            snakeChain.directionX = 0;
            snakeChain.directionY = -1;
            break;
        case "ArrowDown":
            snakeChain.directionX = 0;
            snakeChain.directionY = 1;
            break;
    }
}

/*------------------------------
Algos
*/

function snakeNode() {
    this.x;
    this.y;
    this.next = null;
}

var poop = {
    poopToCome: new Array(),
    treeToCome: new Array(),
    initialize: function() {
        this.poopToCome = new Array();
        this.treeToCome = new Array();
    },
    poopNow: function() {
        for(let i=0; i<this.poopToCome.length; i++) {
            if(this.poopToCome[i] == 4 || (actualLevel == 3 && this.poopToCome[i] == 0)) {
                let audio = new Audio('sound/flush.ogg');
                audio.play();
            }
            if(this.poopToCome[i] == 0 || (actualLevel == 9 && powerUp.power == "diarrhea" && powerUp.onGoing)) return true;
        }
        return false;
    },
    addPoop: function(x, y) {
        mat[x][y] = "poop";
        this.poopToCome.filter(p => p > 0);
        let obj = {"timer": timeToTree, "x": x, "y": y};
        this.treeToCome.push(obj);
    },
    preparePoop: function() {
        this.poopToCome.push(timeToPoop);
        if(actualLevel == 7) {
            this.poopToCome.push(timeToPoop+1);
            this.poopToCome.push(timeToPoop+2);
            this.poopToCome.push(timeToPoop+3);
        }
    },
    advancePoop: function () {
        for(let i=0; i<this.poopToCome.length; i++) {
            this.poopToCome[i]--;
        }
        let filterNecessary = false;
        for(let i=0; i<this.treeToCome.length; i++) {
            this.treeToCome[i].timer--;
            if(this.treeToCome[i].timer == 0) {
                let audio = new Audio('sound/growingTree.ogg');
                audio.play();
                mat[this.treeToCome[i].x][this.treeToCome[i].y] = "tree";
                filterNecessary = true;
                plantedTrees++;
                if(plantedTrees == treesToPlant) win();
            }
        }
        if(filterNecessary) {
            this.treeToCome.filter(e => e.timer > 0);
        }
    },
}

var snakeChain = {
    first: 0,
    directionX: 0,
    directionY: 0,
    outch: false,
    poopInX: new Array(),
    initialize: function() {
        let node = new snakeNode();
        node.x = 5;
        node.y = 5;
        this.first = node;
        mat[node.x][node.y] = "snake";
        this.poopInX = new Array();
        this.directionX = 1;
        this.directionY = 0;
    },
    advance: function() {
        //on avance
        let node = new snakeNode();
        node.x = this.first.x + this.directionX;
        node.y = this.first.y + this.directionY;
        let second = this.first;
        this.first = node;
        node.next = second;
        //on regarde si on sort par un côté
        if(node.x < 0) {
            node.x = mat.length -1;
        } else if(node.x == mat.length) {
            node.x = 0;
        } else if(node.y < 0) {
            node.y = mat[0].length -1;
        } else if(node.y == mat[0].length) {
            node.y = 0;
        }
        //regarder si on mange
        let grow = false;
        if(mat[node.x][node.y] == "cherry") {
            let audio = new Audio('sound/eat.ogg');
            audio.play();
            grow = true;
            cherryGenerated = false;
            mat[node.x][node.y] = "";
            tickSinceEaten = 0;
            poop.preparePoop();
        } else if(mat[node.x][node.y] == "tree" || mat[node.x][node.y] == "snake" || mat[node.x][node.y] == "poop") {
            if(mat[node.x][node.y] == "tree") {  
                plantedTrees--;
            }
            if((actualLevel == 5 && mat[node.x][node.y] == "poop") || (mat[node.x][node.y] == "poop" && actualLevel == 9 && powerUp.power == "vampirism" && powerUp.onGoing) || (mat[node.x][node.y] == "tree" && actualLevel == 9 && powerUp.power == "vegetreerism" && powerUp.onGoing)) {
                if(life<lifeMax) life++;
            } else {
                this.damage();
            }
        } else if(mat[node.x][node.y] == "powerup") {
            let audio = new Audio('sound/powerUp.ogg');
            audio.play();
            powerUp.activate();
        }
        if(!grow) this.eliminateLast();
        mat[node.x][node.y] = "snake";
    },
    eliminateLast: function() {
        let node = this.first;
        while(node.next.next != null) node = node.next;
        poop.advancePoop();
        if(poop.poopNow()) {
            poop.addPoop(node.next.x, node.next.y);
        } else if(actualLevel != 6) {
            mat[node.next.x][node.next.y] = "";
        }
        if(actualLevel != 6) node.next = null;
    },
    damage: function() {
        if(actualLevel == 9 && powerUp.onGoing && powerUp.power == "invincible") return;
        let audio = new Audio('sound/damage.ogg');
        audio.play();
        life--;
        if(life == 0) lose();
        snakeChain.outch = true;
    }
}

function generateFood() {
    if(!cherryGenerated && actualCherries<maxCherries) {
        let x, y;
        let found = false;
		let node;
        while(!found) {
            found = true;
            x = Math.floor(Math.random() * mat.length);
            y = Math.floor(Math.random() * mat[0].length);
            if(mat[x][y] == "") {
                node = snakeChain.first;
                while(node != null && found) {
                    if(node.x == x && node.y == y) {
                        found = false;
                    }
                    node = node.next;
                }
            } else {
                found = false;
            }
        }
        mat[x][y] = "cherry";
        cherryGenerated = true;
    } else {
        //générer de la nouvelle nourriture si ça fait 100 ticks qu'on a pas mangé
        if(actualCherries < maxCherries) {
            tickSinceEaten++;
            if(tickSinceEaten >= tickUntilCherry) {
                cherryGenerated = false;
                tickSinceEaten = 0;
                actualCherries++;
            }
        }
    }
}

var powerUp = {
    activated: false,
    onGoing: false,
    timer: 0,
    power: "",
    initialize: function() {
        this.activated = false;
        this.onGoing = false;
        timerTextPowerUp = 0;
    },
    advance: function() {
        if(!this.activated && !this.onGoing) {
            tickSincePowerUp++;
            if(tickSincePowerUp > 20) {
                this.generate();
                tickSincePowerUp = 0;
            }
        } 
        if(this.onGoing) {
            timerTextPowerUp--;
            this.timer--;
            if(this.timer < 0) {
                this.onGoing = false;
                switch(this.power) {
                    case "faster":
                        refresh = 150;
                        break;
                    case "raining":
                        tickUntilCherry = 100;
                        maxCherries = 4;
                        break;
                    case "diarrhea":
                        break;
                    case "slower":
                        refresh = 150;
                        break;
                    case "invincible":
                        break;
                    case "vampirism":
                        break;
                    case "vegetreerism":
                        break;
                }
            }
        }
    },
    generate: function() {
        this.activated = true;
        let x, y;
        let found = false;
		let node;
        while(!found) {
            found = true;
            x = Math.floor(Math.random() * mat.length);
            y = Math.floor(Math.random() * mat[0].length);
            if(mat[x][y] == "") {
                node = snakeChain.first;
                while(node != null && found) {
                    if(node.x == x && node.y == y) {
                        found = false;
                    }
                    node = node.next;
                }
            } else {
                found = false;
            }
        }
        mat[x][y] = "powerup";
    },
    activate: function() {
        this.activated = false;
        this.onGoing = true;
        this.timer = 70;
        this.power = "";
        timerTextPowerUp = 10;
        let rand = Math.floor(Math.random() * 100);
        if(rand < 10) {
            textPowerUp = "+1 life";
            if(life < lifeMax) life++;
        } else if(rand < 20){
            textPowerUp = "+1 max life";
            if(lifeMax < 8) lifeMax++;
        } else if(rand < 30) {
            textPowerUp = "FASTER";
            this.power = "faster";
            refresh = 50;
        } else if(rand < 40) {
            textPowerUp = "Raining Cherries";
            this.power = "raining";
            tickUntilCherry = 1;
            maxCherries = 150;
        } else if(rand < 50) {
            textPowerUp = "Diarrhea";
            this.power = "diarrhea";
        } else if(rand < 60) {
            textPowerUp = "SLOWER";
            this.power = "slower";
            refresh = 300;
        } else if(rand < 70) {
            textPowerUp = "Invincible";
            this.power = "invincible";
        } else if(rand < 80) {
            textPowerUp = "Poop Vampirism";
            this.power = "vampirism";
        } else if(rand < 90) {
            textPowerUp = "Vegetreerism";
            this.power = "vegetreerism";
        } else {
            textPowerUp = "Full life!";
            life = lifeMax;
        }
    }
}

/*------------------------------
Victoire / défaite
*/

function win() {
    exit = true;
    lost = false;
    timerExit = 2000;
    if(actualLevel == unlockedLevel && unlockedLevel < maxLevel+1) {
        unlockedLevel++;
    }
}

function lose() {
    exit = true;
    lost = true;
    timerExit = 2000;
    if(actualLevel == 9 && plantedTrees > bestScore) {
        bestScore = plantedTrees;
    }
}

/*------------------------------
Menu
*/

function menu() {
    initializeScreen();
    context.fillStyle = "green";
    context.fillRect(0, 0, 700, 700);
    context.font = "50px Arial";
    context.fillStyle = "black";
    context.textAlign = "center";
    context.fillText("-----[Snake]-----", canvas.width/2, 100);

    for(let i=0; i<4; i++) {
        for(let j=0; j<2; j++) {
            let lvl = (i+1)+(j*4);
            if(lvl <= unlockedLevel) {
                let div = document.createElement('div');
                div.style="position: absolute; border-left: 3px solid black; border-bottom: 3px solid black; padding-left: 5px; left: " + (75+j*300) + "px; top: " + (250+ i*75) + "px; height: 35px; width: 250px; text-align: center; background: transparent; font-size: 30px";
                switch(lvl) {
                    case 1:
                        div.textContent = "Humble Beginning";
                        break;
                    case 2:
                        div.textContent = "Plant More";
                        break;
                    case 3:
                        div.textContent = "Instant Pooping";
                        break;
                    case 4:
                        div.textContent = "Ultra Speed";
                        break;
                    case 5:
                        div.textContent = "Poop Vampirism";
                        break;
                    case 6:
                        div.textContent = "Unlimited Growth";
                        break;
                    case 7:
                        div.textContent = "Fiber Diet";
                        break;
                    case 8:
                        div.textContent = "Boxed In";
                        break;
                }
                div.addEventListener("mousedown", e=>{clickOnMenu(lvl)});
                document.getElementsByTagName('body')[0].appendChild(div);
            }
        }
    }
    if(unlockedLevel > maxLevel) {
        let div = document.createElement('div');
        div.style="position: absolute; border-left: 3px solid black; border-bottom: 3px solid black; padding-left: 5px; left: 225px; top: 550px; height: 35px; width: 250px; text-align: center; background: transparent; font-size: 30px";
        div.textContent = "Endless";
        div.addEventListener("mousedown", e=>{clickOnMenu(9)});
        document.getElementsByTagName('body')[0].appendChild(div);
        if(bestScore > 0) {
            let divv = document.createElement('span');
            divv.style="position: absolute;padding-left: 5px; left: 225px; top: 600px; height: 35px; width: 250px; text-align: center; background: transparent; font-size: 30px";
            divv.textContent = "Best score: " + bestScore;
            document.getElementsByTagName('body')[0].appendChild(divv);
        }
    }
    lastTime = 0;
    exitMenu = false;
    menuAnimation(performance.now());
}

function menuAnimation(timestamp) {
    if(lastTime == 0) lastTime = timestamp-1;
    if(timestamp - lastTime >= refresh || timestamp - lastTime == 0) { 
        timer += timestamp - lastTime;
        lastTime = timestamp;
        if(!exitMenu) {
            //dessiner le serpent
            for(let i=0; i<700; i = i + 50) {
                if(i < 150 || i > 500) {
                    context.drawImage(imgSnakeTrail, i, 62, 50, 50);
                } else if(i < 350) {
                    context.drawImage(imgSnakeTrail, i, 112, 50, 50);
                } else if(i > 350) {
                    context.drawImage(imgSnakeTrail, i, 12, 50, 50);
                }
            }
            context.drawImage(imgSnakeTrail, 100, 112, 50, 50);
            context.drawImage(imgSnakeTrail, 550, 12, 50, 50);
            context.drawImage(imgSnakeTrail, 300, 162, 50, 50);
            context.drawImage(imgSnakeHeadRight, 350, 162, 50, 50);
        }
    }
    if(!exitMenu) {
        window.requestAnimationFrame(menuAnimation);
    }
}

/*------------------------------
Démarrage de la page
*/

window.onload = function() {
    initializeImg();
    menu();
}