/*------------------------------
GOALS

/*------------------------------
BUGS

/*------------------------------
Variables globales
*/ 

//canvas layers
var canvasBackground = document.getElementById('canvasBackground');
var ctxBG = canvasBackground.getContext('2d');
var canvasFront = document.getElementById('canvasFront');
var ctxFront = canvasFront.getContext('2d');

//mouse
var canvasPosition = canvasFront.getBoundingClientRect();
const mouse = {
    x: undefined,
    y: undefined,
};
//time
const refresh = 10;
var lastTime = 0;
var timer;
//img
var img = {};
var sound = {};
//import/export
var importExport = document.getElementById("importExport");
var infoImportExport = document.getElementById("infoImportExport");
var contentImportExport = document.getElementById("contentImportExport");


/*------------------------------
Classes
*/

/**
 * Generic upgrade class
 * ---------------------
 * methods: 
 * buy() -> void  
 * sliceMyTooltip() -> Array of strings (slice tooltips in lines)  
 */
class Upgrade {
    /**
     * @param {str} name identifier
     * @param {num} cost 
     * @param {num} costAugmentation linear multiplication
     * @param {num} max 1 if unique upgrade
     * @param {bool} now effect now or later
     * @param {str} text front text
     * @param {str} tooltip 
     * @param {str} img 
     */
    constructor(name, cost, costAugmentation, max, now, text, tooltip, img) {
        this.name = name;
        this.cost = cost;
        this.costAugmentation = costAugmentation;
        this.max = max;
        this.current = 0;
        this.visible = false;
        //display when currency attain 0.7x the cost
        this.visibleRatio = 0.7; 
        this.text = text;
        this.tooltip = tooltip;
        this.now = now;
        this.img = img;
    }
    /**
     * add to current, augment cost, change visibility
     */
    buy() {
        this.current++;
        this.cost = Math.round(this.cost*this.costAugmentation);
        this.visible = false;
    }
    /**
     * cut the tooltip in array of strings
     * @returns Array of strings
     */
    sliceMyTooltip() {
        let toolSlice = new Array();
        const maxLength = 45;
        const offset = 15;
        let min = 0;
        let max = min + maxLength-offset;
        while(max < this.tooltip.length) {
            if(this.tooltip[max] === " ") {
                toolSlice.push(this.tooltip.slice(min, max));
                min = max + 1;
                max = min + maxLength - offset;
            } else {
                max++;
            }
        }
        toolSlice.push(this.tooltip.slice(min));
        return toolSlice;
    }
}

/**
 * Generic button class
 * --------------------
 * methods:  
 * clic -> string if clic inside / undefined if clic outside 
 * draw() -> void
 */
class Button {
    /**
     * @param {num} x left
     * @param {num} y top
     * @param {num} w width
     * @param {num} h height
     * @param {bool} clickable if true, show little corners 
     * @param {string} color background color (default yellow)
     * @param {context} ctxParam layer onto we draw (default to front)
     * @param {bool} visible
     * @param {str} effect name of the effect provocked by a clic
     * @param {str} text
     * @param {num} fontSize
     * @param {str} fontColor
     */
    constructor(x,y,w,h,clickable,color,ctxParam, visible, effect, text, fontSize, fontColor) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.clickable = clickable;
        this.color = color;
        this.ctx = ctxParam;
        this.visible = visible;
        this.effect = effect;
        this.text = text;
        this.fontSize = fontSize;
        this.fontColor = fontColor;
    }
    /**
     * @param {num} x position x of the mouse
     * @param {num} y position y of the mouse
     * @returns {str} if clic reached, else undefined
     */
    clic(x,y) {
        if(this.visible && x >= this.x && x <= this.x+this.w && y > this.y && y < this.y+this.h)
            return this.effect;
        else
            return undefined;
    }
    draw() {
        if(this.visible) {
            //contour
            this.ctx.lineWidth = 3;
            this.ctx.fillStyle = "rgba(150, 40, 27, 1)";
            this.ctx.strokeStyle = "black";
            this.ctx.fillRect(this.x, this.y, this.w, this.h);
            this.ctx.strokeRect(this.x, this.y, this.w, this.h);
            let rgba;
            switch(this.color) {
                case "red":
                    rgba = "rgba(240, 52, 52, 1)";
                    break;
                case "green":
                    rgba = "rgba(30, 130, 76, 1)";
                    break;
                case "grey":
                    rgba = "rgba(109, 113, 122, 1)";
                    break;
                case "lightgrey":
                    rgba = "rgba(228, 233, 237, 1)";
                    break;
                case "brown":
                    rgba = "rgba(164, 116, 73, 1)";
                    break;
                case "yellow2":
                    rgba = "rgba(245, 215, 110, 1)";
                    break;
                case "yellow":
                default:
                    rgba = "rgba(247, 202, 24, 1)";
                    break;
            }
            this.ctx.fillStyle = rgba;
            this.ctx.fillRect(this.x+5, this.y+5,this.w-10,this.h-10);
            this.ctx.strokeRect(this.x+5, this.y+5,this.w-10,this.h-10);
            if(this.clickable) {
                this.ctx.fillStyle = "rgba(108, 122, 137, 1)";
                this.ctx.lineWidth = 5;
                this.ctx.beginPath();
                this.ctx.arc(this.x+2.5,this.y+2.5,5,0,2*Math.PI);
                this.ctx.stroke();
                this.ctx.fill();
                this.ctx.beginPath();
                this.ctx.arc(this.x+this.w-2.5,this.y+this.h-2.5,5,0,2*Math.PI);
                this.ctx.stroke();
                this.ctx.fill();
                this.ctx.beginPath();
                this.ctx.arc(this.x+2.5,this.y+this.h-2.5,5,0,2*Math.PI);
                this.ctx.stroke();
                this.ctx.fill();
                this.ctx.beginPath();
                this.ctx.arc(this.x+this.w-2.5,this.y+2.5,5,0,2*Math.PI);
                this.ctx.stroke();
                this.ctx.fill();
            }
            //text

        }
    }
}

/*------------------------------
Handlers
*/

var gameHandler = {
    state: "loading",
    /* list all states possible
       loading
    */
    amIWorking: false, //prevent too much callbacks
    assetsLoaded: 0, //count images loaded
    loading: true,

    initialize: function() {
        if(this.state == "loading") {       
            //loading assets
            this.initializeImg();
            soundHandler.initialize();
        }

        //other handlers
        UIHandler.initialize();
        upgradeHandler.initialize();

        //loading save
        saveHandler.load();
    },
    /**
     * Init all images:  
     * onload call assetsLoading()
     */
    initializeImg: function() {

    },
    assetsLoading: function() {
        this.assetsLoaded++;
    },
    /**
     * @returns bool
     */
    allAssetsLoaded: function() {
        return this.assetsLoaded == (Object.keys(img).length + Object.keys(sound).length);
        //add here other types of assets
    },
    
    draw: function() {
        //clear front
        ctxFront.clearRect(0,0,canvasFront.width, canvasFront.height);

        //call all draw methods as necessary
    },

    /**
     * main loop, launch this.core() each time
     * @param {time} timestamp 
     */
    animate: function(timestamp) {
        if(lastTime == 0) lastTime = timestamp-1;
        if(timestamp - lastTime >= refresh || timestamp - lastTime == 0) {
            if(!this.amIWorking) {
                //console.time("core");
                this.core();
                //console.timeEnd("core");
                timer += timestamp - lastTime;
                lastTime = timestamp;
            }
        }
        window.requestAnimationFrame(this.animate);
    },

    core: function() {
        this.amIWorking = true;
        if(this.state === "loading") {
            if(this.allAssetsLoaded()) {
                //this.state = "change here"
            } else {
                //show loading advancement
            }
        } else {
            //do things here
            this.draw();
            saveHandler.save();
            upgradeHandler.actualiseVisibility();
        }
        this.amIWorking = false;
    }
};

var upgradeHandler = {
    //contains upgrades objects
    upgradeList: new Array(), 
    //contains names of upgrades that effects later
    upgradeToCome: new Array(), 
    mouseOn: undefined,
    newUpgrade: false,

    /*  list upgrades:

    */

    initialize: function() {
        this.upgradeList = new Array();
        this.upgradeToCome = new Array();
        this.mouseOn = undefined;
        this.newUpgrade = false;
        //to add new Upgrades
        //this.upgradeList.push(new Upgrade(name, cost, costAug, max, now, text, tooltip, img));
    },
    /**
     * recreate list of upgrades after loading (to transform anonym objects from JSON.parse() into Upgrade objects)
     */
    reload: function() {
        let newList = new Array();
        for(let i=0; i<this.upgradeList.length; i++) {
            let u = this.upgradeList[i];
            let u2 = new Upgrade(u.name, u.cost, u.costAugmentation, u.max, u.now, u.text, u.tooltip, u.img);
            u2.current = u.current;
            u2.visible = u.visible;
            u2.visibleRatio = u.visibleRatio;
            newList.push(u2);
        }
        this.upgradeList = newList;
    },
    /**
     * check if enough currency to buy  
     * update currency  
     * effect now or place info for later  
     * @param {Upgrade} upgrade 
     */
    buy: function(upgrade) {
        if(upgrade.cost <= 1 /*gameHandler.money*/) {
            //here change currency handling as fit
            //gameHandler.money -= upgrade.cost;
            upgrade.buy();
            if(upgrade.now)
                this.effect(upgrade.name);
            else   
                this.upgradeToCome.push({name: upgrade.name, text: upgrade.text});
                //here can change pushing method to count {name, text, count}
        }
    },
    /**
     * switch the upgrade names  
     * effect accordingly
     * @param {str} name 
     */
    effect: function(name) {
        switch(name) {
            default:
                break;
        }
    },
    /**
     * effect the upgrades in the upgradeToCome list
     */
    effectUpToCome: function() {
        for(let i=0; i<this.upgradeToCome.length; i++) {
            this.effect(this.upgradeToCome[i].name);
        }
        this.upgradeToCome = new Array();
    },
    /**
     * check all upgrades to see which to display
     * @returns array of Upgrades
     */
    listBuyables: function() {
        return this.upgradeList.filter(e => e.visible);
    },
    /**
     * check the list of upgrades against currency  
     * set this.newUpgrade flag to true if an upgrade becomes visible
     */
    actualiseVisibility: function() {
        for(let i=0; i<this.upgradeList.length; i++) {
            let up = this.upgradeList[i];
            //here change for the currency employed
            let gold = 1; //gameHandler.money;
            if(!up.visible && up.current < up.max && up.cost*up.visibleRatio < gold) {
                up.visible = true;
                //flag that there is a new upgrade in town
                this.newUpgrade = true;
            }
        }
    },

    draw: function() {
        
    },
};

var soundHandler = {
    musicOn: true,

    initialize: function() {
        //start on
        this.musicOn = true;
        this.initializeSound();
    },
    initializeSound: function() {
        /*
        this.sourceMusic = new Audio("sound/music1genericbackgroundtrack.mp3");
        this.sourceMusic.loop = true;
        this.sourceMusic.load();
        this.sourceMusic.addEventListener("load", function() {
            gameHandler.assetsLoading();
        }, true);
        */
    },

    changeHearabilityMusic: function() {
        this.musicOn = !this.musicOn;
        this.actualiseMusic();
    },
    actualiseMusic: function() {
        if(gameHandler.state != "loading") {
            if(this.musicOn) {
                //this.sourceMusic.play();
            } else {
                //this.sourceMusic.pause();
            }
        }
    }
};

var saveHandler = {
    file: undefined,
    countToSave: 500, //how many frames before automatic save
    importOpen: false,

    load: function() {
        this.file = JSON.parse(localStorage.getItem(/* name */"a"));
        if(this.verifySave()) {
            
            //upgrades
            Object.assign(upgradeHandler.upgradeList, this.file.upgrade.upgradeList);
            Object.assign(upgradeHandler.upgradeToCome, this.file.upgrade.upgradeToCome);
            upgradeHandler.newUpgrade = this.file.upgrade.newUpgrade;
            //sound
            soundHandler.musicOn = this.file.sound.musicOn;
            
            upgradeHandler.reload();
        }
        this.createFile();
    },
    save: function() {
        this.countToSave--;
        if(this.countToSave <= 0) {
            this.countToSave = 500;
            this.createFile();
            localStorage.setItem(/* name */"a", JSON.stringify(this.file));
        }
    },
    createFile: function() {
        this.file = {
            "game": {

            },
            "UI": {
                
            },
            "upgrade": {
                "upgradeList": upgradeHandler.upgradeList,
                "upgradeToCome": upgradeHandler.upgradeToCome,
                "newUpgrade": upgradeHandler.newUpgrade,
            },
            "sound": {
                "musicOn": soundHandler.musicOn,
            }
        };
    },
    erase: function() {
        localStorage.removeItem(/* name */"a");

        gameHandler.initialize();
    },
    verifySave: function() {
        return (
            this.file !== undefined && 
            this.file !== null && 
            this.file.game !== undefined && 
            this.file.upgrade !== undefined && 
            this.file.sound !== undefined
            );
    },
    import: function() {
        if(this.importOpen) {
            let save = contentImportExport.value;
            try {
                this.file = JSON.parse(window.atob(save));
                if(this.verifySave()) {
                    localStorage.setItem(/* name */"a", JSON.stringify(this.file));
                    this.countToSave = 500;
                    this.load();
                    this.openImportExport("Save successfully imported");
                }
            } catch(err) {
                contentImportExport.value = err;
            }
        } else {
            this.openImportExport("Import");
            this.importOpen = true;
        }
    },
    export: function() {
        this.createFile();
        this.openImportExport("Copied to clipboard");
        let str = window.btoa(JSON.stringify(this.file));
        contentImportExport.value = str;
        contentImportExport.select();
        document.execCommand("copy");
    },
    openImportExport: function(message) {
        importExport.className = "notHidden";
        infoImportExport.textContent = message;
    },
    closeImportExport: function() {
        importExport.className = "hidden";
        this.importOpen = false;
        contentImportExport.value = "Paste here your code, then click import";
    },

};

var UIHandler = {
    button: new Array(),

    initialize: function() {
        this.button = new Array();
        //this.button.push(new Button(x,y,w,h,clickable,color,ctxParam, visible, effect, text, fontSize, fontColor))
        //here declare all buttons and menus

    },
    clic: function() {
        let x = mouse.x;
        let y = mouse.y;
        let i=0;
        //to stop when a clic is performed
        let clicEffect = false;
        while(i<this.button.length && !clicEffect) {
            let effectName = this.button[i].clic();
            if(effectName !== undefined) {
                //here effect the clic

                clicEffect = true;
            }
            i++;
        }
    },
    draw: function() {
        for(let i=0; i<this.button.length; i++) {
            this.button[i].draw();
        }
    },
};

/*------------------------------
Interaction utilisateur
*/

document.addEventListener('mousemove', e => {
    canvasPosition = canvas.getBoundingClientRect();
    mouse.x = Math.round(e.x - canvasPosition.left);
    mouse.y = Math.round(e.y - canvasPosition.top);
    //what to do with that?
});

document.addEventListener('mousedown', e => {mouseDown()});
document.addEventListener('mouseup', e => {mouseUp()});

function mouseDown() {
    UIHandler.clic();
}

function mouseUp() {

}

/*------------------------------
Démarrage de la page
*/

window.onload = function() {
    gameHandler.initialize();
}