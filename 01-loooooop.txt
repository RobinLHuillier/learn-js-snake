Ok c'est pas beau ça fait peur. Avec l'explication c'est pas si pire.
refresh -> nombre de millisecondes entre deux tours de boucle (100ms == 10 lancements par seconde)
window.requestAnimationFrame(maFonction) -> lance maFonction avec en paramètre le timestamp de quand ça a été lancé
	-> relance maFonction autant de fois que le navigateur et l'ordi le permettent (trop souvent pour nous)
	-> avantage par rapport à setInterval : bien plus stable
Décortiquer l'intérieur d'animate (permet de calculer si suffisamment de temps s'est écoulé, si oui on lance le core(), sinon on dit tant pis).
console.time/timeEnd -> permet de vérifier si notre refresh est pas trop petit (ou notre programme est pas devenu trop gros)

/*------------------------------
Variables globales
*/

#### AJOUT ####

//looping animation
var refresh = 100;
var lastTime = 0;
var timer;


/*------------------------------
Animation and interactions
*/

#### AJOUT ####

function animate(timestamp) {
    if(lastTime == 0) lastTime = timestamp-1;
    if(timestamp - lastTime >= refresh || timestamp - lastTime == 0) {
        //console.time("core");
        gameHandler.core();
        //console.timeEnd("core");
        timer += timestamp - lastTime;
        lastTime = timestamp;
    }
    window.requestAnimationFrame(animate);
}